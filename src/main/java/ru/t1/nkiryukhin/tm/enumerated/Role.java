package ru.t1.nkiryukhin.tm.enumerated;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private final String displayName;

    public String getDisplayName() {
        return displayName;
    }

    Role(String displayName) {
        this.displayName = displayName;
    }

}
