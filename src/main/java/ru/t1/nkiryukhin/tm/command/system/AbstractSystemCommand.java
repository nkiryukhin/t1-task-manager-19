package ru.t1.nkiryukhin.tm.command.system;

import ru.t1.nkiryukhin.tm.api.service.ICommandService;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
