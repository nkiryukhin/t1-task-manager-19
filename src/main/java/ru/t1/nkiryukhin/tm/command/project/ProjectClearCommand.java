package ru.t1.nkiryukhin.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        getProjectService().clear();
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

}
