package ru.t1.nkiryukhin.tm.command.task;

import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Display all tasks";
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTasks(tasks);
    }

}
