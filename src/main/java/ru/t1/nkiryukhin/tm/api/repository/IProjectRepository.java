package ru.t1.nkiryukhin.tm.api.repository;

import ru.t1.nkiryukhin.tm.model.Project;


public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

    Project create(String name);

}