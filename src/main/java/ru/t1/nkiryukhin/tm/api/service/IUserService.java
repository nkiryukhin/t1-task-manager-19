package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.EmailEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.LoginEmptyException;
import ru.t1.nkiryukhin.tm.model.User;


public interface IUserService extends IService<User> {

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User findByLogin(String login) throws LoginEmptyException;

    User findByEmail(String email) throws EmailEmptyException;

    User removeByLogin(String login) throws AbstractException;

    User removeByEmail(String email) throws AbstractException;

    User setPassword(String id, String password) throws AbstractException;

    User updateUser(String id, String firstName, String lastName, String middleName) throws AbstractException;

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}